/**
 * @file
 * A bigger and better admin_menu
 */

(function($) {
  Drupal.admin = Drupal.admin || {};
  Drupal.admin.behaviors = Drupal.admin.behaviors || {};

  /**
   * Show a small menu button when the menu is too large
   */
  Drupal.admin.behaviors.blueBarResize = function (context, settings, $adminMenu) {
    function updateToolbar() {
      var $menu = $('#admin-menu-menu', $adminMenu);
      var $dropDowns = $('#admin-menu-wrapper > ul.dropdown:not(#admin-menu-menu) > li', $adminMenu);
      var width = 0;

      $dropDowns.each(function() {
        width += $(this).width();
      });

      if ($adminMenu.width() - width > $menu.width() + 40) {
        $adminMenu.addClass('admin-menu-show-menu');
      }
      else {
        $adminMenu.removeClass('admin-menu-show-menu');
      }
      setTimeout(updateToolbar, 500);
    }
    updateToolbar();
  };

  Drupal.admin.behaviors.blueBarSearchFocus = function (context, settings, $adminMenu) {
    $('input.admin-menu-search', $adminMenu).hover(function(){
      $(this).focus();
    });
    $('input.admin-menu-search', $adminMenu).blur(function(){
      $('.admin-menu-search-results', $adminMenu).remove();
    });
  };

  Drupal.admin.behaviors.blueBarHoverParent = function (context, settings, $adminMenu) {
    $('.dropdown a, .dropdown span').hover(function() {
     $(this).parents('li').addClass('opened');
    },
    function() {
       $(this).parents('li').removeClass('opened');
    });
  };

})(jQuery);
