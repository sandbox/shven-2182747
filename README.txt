Quick info:
“Bluebar” changes the style of the “Administration menu” module. 
It adds a nice and simple minimalist look and improves the user experience.

Requirements:
Requires the administration menu module (http://drupal.org/project/admin_menu) 

Installation:
1) Download and install like any other drupal module
2) Enable “Bluebar” from the module manager. 
If you are using the “Administration menu Toolbar style” it will be disabled
automatically to prevent some styling issues. This change will be saved in your
database, so if you decide to disable “Bluebar”
your old “Administration menu Toolbar style” will be enabled automatically
so you have nothing to worry about.
3) Refresh your administration page and you must be good to go!
